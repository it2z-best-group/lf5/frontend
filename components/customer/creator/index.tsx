import { Button, Container, TextField } from "@mui/material";
import axios from "axios";
import { useState } from "react";

export default function CustomerCreator() {
  const [customerData, setCustomerData] = useState<CustomerData>({
    firstName: "",
    lastName: "",
    email: "",
    telephoneNumber: "",
    city: "",
    zip: "",
    street: "",
    houseNumber: "",
    birthDate: new Date(),
  });
  function handleSubmit() {
    axios.post("/api/customer", customerData);
    setCustomerData({
      ...customerData,
      firstName: "",
      lastName: "",
    });
  }

  return (
    <Container>
      <br />
      <br />
      <TextField
        onChange={(e) =>
          setCustomerData({
            ...customerData,
            firstName: e.target.value,
          })
        }
        value={customerData.firstName}
        label="Vorname"
        variant="outlined"
      />
      <br />
      <br />
      <TextField
        onChange={(e) =>
          setCustomerData({
            ...customerData,
            lastName: e.target.value,
          })
        }
        value={customerData.lastName}
        label="Nachname"
        variant="outlined"
      />
      <br />
      <br />
      <TextField
        onChange={(e) =>
          setCustomerData({
            ...customerData,
            email: e.target.value,
          })
        }
        value={customerData.email}
        label="E-Mail"
        variant="outlined"
      />
      <br />
      <br />
      <TextField
        onChange={(e) =>
          setCustomerData({
            ...customerData,
            telephoneNumber: e.target.value,
          })
        }
        value={customerData.telephoneNumber}
        label="Telefon Nummer"
        variant="outlined"
      />
      <br />
      <br />
      <TextField
        onChange={(e) =>
          setCustomerData({
            ...customerData,
            city: e.target.value,
          })
        }
        value={customerData.city}
        label="Stadt"
        variant="outlined"
      />
      <br />
      <br />
      <TextField
        onChange={(e) =>
          setCustomerData({
            ...customerData,
            zip: e.target.value,
          })
        }
        value={customerData.zip}
        label="Postleitzahl"
        variant="outlined"
      />
      <br />
      <br />
      <TextField
        onChange={(e) =>
          setCustomerData({
            ...customerData,
            street: e.target.value,
          })
        }
        value={customerData.street}
        label="Straße"
        variant="outlined"
      />
      <br />
      <br />
      <TextField
        onChange={(e) =>
          setCustomerData({
            ...customerData,
            houseNumber: parseInt(e.target.value.replace(/[^\d]/g, "")) || "",
          })
        }
        value={customerData.houseNumber}
        label="Hausnummer"
        variant="outlined"
      />
      <br />
      <br />
      Birthday:{" "}
      <input
        type="date"
        value={customerData.birthDate.toISOString().substring(0, 10)}
        onChange={(e) =>
          e.target.value &&
          setCustomerData({
            ...customerData,
            birthDate: new Date(e.target.value),
          })
        }
      />
      <br />
      <br />
      <Button
        disabled={Object.values(customerData).some((x) => !x)}
        variant="outlined"
        onClick={handleSubmit}
      >
        Submit
      </Button>
    </Container>
  );
}
