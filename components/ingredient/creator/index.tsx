//@ts-nocheck
import {
  Button,
  Container,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from "@mui/material";
import axios from "axios";
import { useState } from "react";
import useSWR from "swr";

export default function IngredientCreator() {
  const { data: supplier } = useSWR<{ id: number; name: string }[]>(
    "/api/supplier",
    (url) => axios.get(url).then((res) => res.data)
  );
  const [ingredientData, setIngredientData] = useState<Ingredient>({
    supplierId: "",
    name: "",
    unit: "",
    price: "",
    stock: "",
    calories: "",
    carbohydrates: "",
    protein: "",
    fat: "",
    fiber: "",
    sodium: "",
  });
  function handleSubmit() {
    axios.post("/api/ingredients", ingredientData);
    setIngredientData({
      ...ingredientData,
      name: "",
    });
  }
  return (
    <Container>
      <br />
      <br />
      <FormControl sx={{ width: "225px" }}>
        <InputLabel>Lieferant</InputLabel>
        <Select
          value={ingredientData.supplierId}
          label="Lieferant"
          placeholder="Lieferant"
          onChange={(e) =>
            setIngredientData({
              ...ingredientData,
              supplierId: e.target.value as number,
            })
          }
        >
          {supplier?.map((supp, i) => (
            <MenuItem value={supp.id} key={i}>
              {supp.name}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
      <br />
      <br />
      <TextField
        onChange={(e) =>
          setIngredientData({
            ...ingredientData,
            name: e.target.value,
          })
        }
        value={ingredientData.name}
        label="Name"
        variant="outlined"
      />
      <br />
      <br />
      <TextField
        onChange={(e) =>
          setIngredientData({
            ...ingredientData,
            unit: e.target.value,
          })
        }
        value={ingredientData.unit}
        label="Einhheit"
        variant="outlined"
      />
      <br />
      <br />
      <TextField
        onChange={(e) =>
          setIngredientData({
            ...ingredientData,
            calories: parseInt(e.target.value.replace(/[^\d]/g, "")) || "",
          })
        }
        value={ingredientData.calories}
        label="Kalorien"
        variant="outlined"
      />
      <br />
      <br />
      <TextField
        onChange={(e) =>
          setIngredientData({
            ...ingredientData,
            carbohydrates: parseInt(e.target.value.replace(/[^\d]/g, "")) || "",
          })
        }
        value={ingredientData.carbohydrates}
        label="Kohlenhydrate"
        variant="outlined"
      />
      <br />
      <br />
      <TextField
        onChange={(e) =>
          setIngredientData({
            ...ingredientData,
            protein: parseInt(e.target.value.replace(/[^\d]/g, "")) || "",
          })
        }
        value={ingredientData.protein}
        label="Protein"
        variant="outlined"
      />
      <br />
      <br />
      <TextField
        onChange={(e) =>
          setIngredientData({
            ...ingredientData,
            fat: parseInt(e.target.value.replace(/[^\d]/g, "")) || "",
          })
        }
        value={ingredientData.fat}
        label="fat"
        variant="outlined"
      />
      <br />
      <br />
      <TextField
        onChange={(e) =>
          setIngredientData({
            ...ingredientData,
            fiber: parseInt(e.target.value.replace(/[^\d]/g, "")) || "",
          })
        }
        value={ingredientData.fiber}
        label="fiber"
        variant="outlined"
      />
      <br />
      <br />
      <TextField
        onChange={(e) =>
          setIngredientData({
            ...ingredientData,
            sodium: parseInt(e.target.value.replace(/[^\d]/g, "")) || "",
          })
        }
        value={ingredientData.sodium}
        label="sodium"
        variant="outlined"
      />
      <br />
      <br />
      <TextField
        onChange={(e) =>
          setIngredientData({
            ...ingredientData,
            price: parseInt(e.target.value.replace(/[^\d]/g, "")) || "",
          })
        }
        value={ingredientData.price}
        label="Preis €"
        variant="outlined"
      />
      <br />
      <br />
      <TextField
        onChange={(e) =>
          setIngredientData({
            ...ingredientData,
            stock: parseInt(e.target.value.replace(/[^\d]/g, "")) || "",
          })
        }
        value={ingredientData.stock}
        label="Anzahl auf Lager"
        variant="outlined"
      />
      <Button
        disabled={Object.values(ingredientData).some((x) => !x)}
        variant="outlined"
        onClick={handleSubmit}
      >
        Submit
      </Button>
    </Container>
  );
}
