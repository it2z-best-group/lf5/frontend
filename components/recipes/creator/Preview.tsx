import {
  Input,
  OutlinedInput,
  Table,
  TableBody,
  TableCell,
  TableRow,
} from "@mui/material";
import { Container } from "@mui/system";
import { Dispatch, SetStateAction } from "react";

interface Props {
  checkedIngredients: CheckedIngredient[];
  setCheckedIngredients: Dispatch<SetStateAction<CheckedIngredient[]>>;
  recipeName: string;
  setRecipeName: Dispatch<SetStateAction<string>>;
}

export default function Preview({
  checkedIngredients,
  setCheckedIngredients,
  recipeName,
  setRecipeName,
}: Props) {
  function handleAmountChange(
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
    name: string
  ) {
    setCheckedIngredients(
      checkedIngredients.map((ingred) => ({
        ...ingred,
        amount:
          ingred.name == name
            ? parseInt(e.target.value.replace(/[^\d]/g, "")) || ""
            : ingred.amount,
      }))
    );
  }

  return (
    <>
      {checkedIngredients.length ? (
        <OutlinedInput
          placeholder="Recipe Name"
          value={recipeName}
          onChange={(e) => setRecipeName(e.target.value)}
        />
      ) : null}
      <Table>
        <TableBody>
          {checkedIngredients.map((ingredient, i) => (
            <TableRow key={i}>
              <TableCell>{ingredient.name}</TableCell>
              <TableCell>
                <Input
                  color={ingredient.amount ? "success" : "error"}
                  type="tel"
                  value={ingredient.amount}
                  onChange={(e) => handleAmountChange(e, ingredient.name)}
                />
                {ingredient.unit}
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </>
  );
}
