import { Checkbox, FormControlLabel, FormGroup } from "@mui/material";
import { Dispatch, SetStateAction } from "react";

interface Props {
  ingredients: Ingredient[];
  checkedIngredients: CheckedIngredient[];
  setCheckedIngredients: Dispatch<SetStateAction<CheckedIngredient[]>>;
}

export default function Selector({
  ingredients,
  checkedIngredients,
  setCheckedIngredients,
}: Props) {
  function handleCheckbox(
    e: React.ChangeEvent<HTMLInputElement>,
    unit: string,
    id:number
  ) {
    if (e.target.checked) {
      setCheckedIngredients(
        checkedIngredients.concat([{id, name: e.target.value, amount: 1, unit }])
      );
    } else {
      setCheckedIngredients(
        checkedIngredients.filter((v) => v.name !== e.target.value)
      );
    }
  }

  return (
    <FormGroup style={{ display: "flex", flexDirection: "row" }}>
      {ingredients.map((ingredient, i) => (
        <FormControlLabel
          key={i}
          control={
            <Checkbox
              value={ingredient.name}
              onChange={(e) => handleCheckbox(e, ingredient.unit, ingredient.id as number)}
            />
          }
          label={ingredient.name}
        />
      ))}
    </FormGroup>
  );
}
