import { Button, Checkbox, Container, FormControlLabel, FormGroup } from "@mui/material";
import axios from "axios";
import { useState } from "react";
import useSWR from "swr";
import Preview from "./Preview";
import Selector from "./Selector";

export default function RecipeCreator() {
  const { data: ingredients } = useSWR<Ingredient[]>(
    "/api/ingredients",
    (url) => axios.get(url).then((res) => res.data)
  );
  const [checkedIngredients, setCheckedIngredients] = useState<
    CheckedIngredient[]
  >([]);
  const { data: nutritionalCats } = useSWR<{id: number, name: string}[]>("/api/nutritionalCats", (url) =>
    axios.get(url).then((res) => res.data)
  );
  const { data: allergenics } = useSWR<{id: number, name: string}[]>("/api/allergenics", (url) =>
    axios.get(url).then((res) => res.data)
  );
  const [allergenic_ids, setAllergenic_id] = useState<number[]>([]);
  const [nutritionalCat_ids, setNutritionalCat_id] = useState<number[]>([]);

  const [recipeName, setRecipeName] = useState("");

  if (!ingredients) {
    return null;
  }


  function handleCheckbox1(e: React.ChangeEvent<HTMLInputElement>, v: {id: number, name: string}){
    if (e.target.checked) {
      setNutritionalCat_id(
        nutritionalCat_ids.concat([v.id])
      );
    } else {
      setNutritionalCat_id(
        nutritionalCat_ids.filter((v) => v != e.target.value as unknown as number)
      );
    }
  }
  function handleCheckbox2(e: React.ChangeEvent<HTMLInputElement>, v: {id: number, name: string}){
    if (e.target.checked) {
      setAllergenic_id(
        allergenic_ids.concat([v.id])
      );
    } else {
      setAllergenic_id(
        allergenic_ids.filter((v) => v != e.target.value as unknown as number)
      );
    }
  }
  return (
    <Container>
      Zutaten:
      <Selector
        ingredients={ingredients}
        checkedIngredients={checkedIngredients}
        setCheckedIngredients={setCheckedIngredients}
      />
      <br />
      Nährstoff Kategorien:
      <FormGroup style={{ display: "flex", flexDirection: "row" }}>
        {nutritionalCats?.map((nutritionalCat, i) => (
          <FormControlLabel
            key={i}
            control={
              <Checkbox
                value={nutritionalCat.id}
                onChange={(e) => handleCheckbox1(e, nutritionalCat)}
              />
            }
            label={nutritionalCat.name}
          />
        ))}
      </FormGroup>
      <br />
      Allergene:
      <FormGroup style={{ display: "flex", flexDirection: "row" }}>
        {allergenics?.map((allergenic:any, i) => (
          <FormControlLabel
            key={i}
            control={
              <Checkbox
                value={allergenic.name}
                onChange={(e) => handleCheckbox2(e, allergenic)}
              />
            }
            label={allergenic.name}
          />
        ))}
      </FormGroup>
      <br />
      <Preview
        checkedIngredients={checkedIngredients}
        setCheckedIngredients={setCheckedIngredients}
        recipeName={recipeName}
        setRecipeName={setRecipeName}
      />
      <br />
      <Button
        onClick={() =>
          axios.post("/api/recipe", {
            name: recipeName,
            ingredients: checkedIngredients.map(ing => ({id: ing.id, amount: ing.amount})),
            allergenic_ids,
            nutritionalCat_ids,
          })
        }
        variant="outlined"
      >
        Submit
      </Button>
    </Container>
  );
}
