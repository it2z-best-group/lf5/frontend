import { Button, Container, TextField } from "@mui/material";
import axios from "axios";
import { useState } from "react";

export default function SupplierCreator() {
  const [supplierData, setSupplierData] = useState<SupplierData>({
    name: "",
    email: "",
    telephoneNumber: "",
    city: "",
    zip: "",
    street: "",
    houseNumber: "",
  });
  function handleSubmit() {
    axios.post("/api/supplier", supplierData);
    setSupplierData({
      ...supplierData,
      name: "",
    });
  }

  return (
    <Container>
      <br />
      <br />
      <TextField
        onChange={(e) =>
          setSupplierData({
            ...supplierData,
            name: e.target.value,
          })
        }
        value={supplierData.name}
        label="Name"
        variant="outlined"
      />
      <br />
      <br />
      <TextField
        onChange={(e) =>
          setSupplierData({
            ...supplierData,
            email: e.target.value,
          })
        }
        value={supplierData.email}
        label="E-Mail"
        variant="outlined"
      />
      <br />
      <br />
      <TextField
        onChange={(e) =>
          setSupplierData({
            ...supplierData,
            telephoneNumber: e.target.value,
          })
        }
        value={supplierData.telephoneNumber}
        label="Telefon Nummer"
        variant="outlined"
      />
      <br />
      <br />
      <TextField
        onChange={(e) =>
          setSupplierData({
            ...supplierData,
            city: e.target.value,
          })
        }
        value={supplierData.city}
        label="Stadt"
        variant="outlined"
      />
      <br />
      <br />
      <TextField
        onChange={(e) =>
          setSupplierData({
            ...supplierData,
            zip: e.target.value,
          })
        }
        value={supplierData.zip}
        label="Postleitzahl"
        variant="outlined"
      />
      <br />
      <br />
      <TextField
        onChange={(e) =>
          setSupplierData({
            ...supplierData,
            street: e.target.value,
          })
        }
        value={supplierData.street}
        label="Straße"
        variant="outlined"
      />
      <br />
      <br />
      <TextField
        onChange={(e) =>
          setSupplierData({
            ...supplierData,
            houseNumber: parseInt(e.target.value.replace(/[^\d]/g, "")) || "",
          })
        }
        value={supplierData.houseNumber}
        label="Hausnummer"
        variant="outlined"
      />
      <br />
      <br />
      <Button
        disabled={Object.values(supplierData).some((x) => !x)}
        variant="outlined"
        onClick={handleSubmit}
      >
        Submit
      </Button>
    </Container>
  );
}
