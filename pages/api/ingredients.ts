import mysql2 from "mysql2/promise";

import { NextApiRequest, NextApiResponse } from "next";
import nc from "next-connect";
import { createIngredient, getIngredients } from "../../sql/ingredient";

const handler = nc<NextApiRequest, NextApiResponse>({
  onError: (err, req, res, next) => {
    console.error(err.stack);
    res.status(500).end("Something broke!");
  },
  onNoMatch: (req, res) => {
    res.status(404).end("Page is not found");
  },
})
  .get<NextApiRequest, NextApiResponse<Ingredient[]>>(async (req, res) => {
    const ingredients = await getIngredients();
    res.json(ingredients);
  })
  .post(async (req, res) => {
    await createIngredient(req.body as Ingredient);
    res.json({ hello: "world" });
  })
  .put(async (req, res) => {
    res.end("async/await is also supported!");
  })
  .patch(async (req, res) => {
    throw new Error("Throws me around! Error can be caught and handled.");
  });

export default handler;
