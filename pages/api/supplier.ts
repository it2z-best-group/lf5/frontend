import { NextApiRequest, NextApiResponse } from "next";
import nc from "next-connect";
import { createSupplier, getSupplier } from "../../sql/supplier";

const handler = nc<NextApiRequest, NextApiResponse>({
  onError: (err, req, res, next) => {
    console.error(err.stack);
    res.status(500).end("Something broke!");
  },
  onNoMatch: (req, res) => {
    res.status(404).end("Page is not found");
  },
})
  .get(async (req, res) => {
    const supplier = await getSupplier()
    res.json(supplier)
  })
  .post(async (req, res) => {
    await createSupplier(req.body as SupplierData);
    res.json({ hello: "world" });
  })
  .put(async (req, res) => {
    res.end("async/await is also supported!");
  })
  .patch(async (req, res) => {
    throw new Error("Throws me around! Error can be caught and handled.");
  });

export default handler;
