import { Container } from "@mui/material";
import Link from "next/link";

export default function Home() {
  return (
    <Container>
      {/* <RecipeCreator /> */}
      <Link href={"/newcustomer"}>Neuen Kunden erstellen</Link><br />
      <Link href={"/newsupplier"}>Neuen Lieferanten erstellen</Link><br />
      <Link href={"/newingredient"}>Neuen Zutat erstellen</Link><br />
      <Link href={"/newrecipe"}>Neues Rezept erstellen</Link><br />
    </Container>
  );
}
