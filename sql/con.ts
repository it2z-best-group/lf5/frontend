import mysql2 from "mysql2/promise";

async function getConnection() {
  return await mysql2.createConnection({
    host: process.env.DATABASE_HOST,
    user: process.env.DATABASE_USERNAME,
    password: process.env.DATABASE_PASSWORD,
    database: "krautundrueben",
  });
}

export default getConnection;
