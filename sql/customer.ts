import { ResultSetHeader } from "mysql2/promise";
import getConnection from "./con";

export async function createCustomer({
  firstName,
  lastName,
  birthDate,
  street,
  houseNumber,
  zip,
  city,
  telephoneNumber,
  email,
}: any) {
  const connection = await getConnection();

  const [rows] = await connection.execute<ResultSetHeader>(
    `INSERT INTO Address (
        street, 
        house_number, 
        zip, 
        city, 
        telephone_number, 
        email
    )
    VALUES (
        '${street}', 
        ${houseNumber}, 
        '${zip}', 
        '${city}', 
        '${telephoneNumber}', 
        '${email}'
    );`
  );

  await connection.execute(
    `INSERT INTO Customer (
        id,
        first_name, 
        last_name, 
        birth_date
    ) 
    VALUES (
        ${rows.insertId}, 
        '${firstName}', 
        '${lastName}', 
        DATE('${birthDate.slice(0, 19).replace("T", " ")}')
    );`
  );

  connection.destroy();
}
