import { ResultSetHeader } from "mysql2/promise";
import getConnection from "./con";

export async function getIngredients() {
  const con = await getConnection();
  const [rows] = await con.query("SELECT * FROM Ingredient;");
  return rows as Ingredient[];
}

export async function createIngredient({
  supplierId,
  name,
  unit,
  price,
  stock,
  calories,
  carbohydrates,
  protein,
  fat,
  fiber,
  sodium,
}: Ingredient) {
  const connection = await getConnection();

  const [rows] = await connection.execute<ResultSetHeader>(
    `INSERT INTO Ingredient (
      supplier_id,
      name,
      unit,
      price,
      stock
    )
    VALUES (
      ${supplierId},
      '${name}', 
      '${unit}', 
      ${price}, 
      ${stock}
    );`
  );

  await connection.execute(
    `INSERT INTO Nutrition (
      ingredient_id,
      calories,
      protein,
      carbohydrates,
      fat,
      fiber,
      sodium
    )
    VALUES (
      ${rows.insertId},
      ${calories}, 
      ${carbohydrates},
      ${protein},
      ${fat},
      ${fiber},
      ${sodium}
    );`
  );

  connection.destroy();
}
