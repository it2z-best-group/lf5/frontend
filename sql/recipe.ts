import { ResultSetHeader } from "mysql2/promise";
import getConnection from "./con";

export async function createRecipe({
  name,
  ingredients,
  allergenic_ids,
  nutritionalCat_ids,
}: {
  name: string;
  ingredients: { id: number; amount: number }[];
  allergenic_ids: number[];
  nutritionalCat_ids: number[];
}) {
  const connection = await getConnection();
  const [rows] = await connection.execute<ResultSetHeader>(
    `INSERT INTO Recipe(name) VALUES('${name}');`
  );
  for (const ingredient of ingredients)
    await connection.execute(
      `INSERT INTO RecipeAmount(ingredient_id, recipe_id, amount) VALUES(${ingredient.id}, ${rows.insertId}, ${ingredient.amount});`
    );
  for (const nutritionalCat_id of nutritionalCat_ids)
    await connection.execute(
    `INSERT INTO RecipeNutritionalCategories(recipe_id, nutritionalcategories_id) VALUES(${rows.insertId}, ${nutritionalCat_id});`
  );
  for (const allergenic_id of allergenic_ids)
    await connection.execute(
    `INSERT INTO RecipeAllergenic(recipe_id, allergenic_id) VALUES(${rows.insertId}, ${allergenic_id});`
  );
  connection.destroy();
}

export async function getNutritionalCats(){
    const connection = await getConnection();
    const [rows] = await connection.execute("SELECT * FROM NutritionalCategories;")
    connection.destroy();
    return rows
}
export async function getAllergenics(){
    const connection = await getConnection();
    const [rows] = await connection.execute("SELECT * FROM Allergenic;")
    connection.destroy();
    return rows
}
