import { ResultSetHeader, RowDataPacket } from "mysql2/promise";
import getConnection from "./con";

export async function createSupplier({
  name,
  street,
  houseNumber,
  zip,
  city,
  telephoneNumber,
  email,
}: SupplierData) {
  const connection = await getConnection();

  const [rows] = await connection.execute<ResultSetHeader>(
    `INSERT INTO Address (
        street, 
        house_number, 
        zip, 
        city, 
        telephone_number, 
        email
    )
    VALUES (
        '${street}', 
        ${houseNumber}, 
        '${zip}', 
        '${city}', 
        '${telephoneNumber}', 
        '${email}'
    );`
  );

  await connection.execute(
    `INSERT INTO Supplier (
        id,
        name
    ) 
    VALUES (
        ${rows.insertId}, 
        '${name}'
    );`
  );

  connection.destroy();
}

export async function getSupplier(){
  const connection = await getConnection();

  const [rows] = await connection.execute(
    "SELECT * FROM Supplier;"
  )
  connection.destroy();
  return rows
}