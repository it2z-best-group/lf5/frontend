interface Ingredient {
  id: number | "" | null;
  supplierId: number | "";
  name: string;
  unit: string;
  price: number | "";
  stock: number | "";
  calories: number | "";
  carbohydrates: number | "";
  protein: number | "";
  fat: number | "";
  fiber: number | "";
  sodium: number | "";
}

interface CheckedIngredient {
  id : number,
  name: string;
  unit: string;
  amount: number | "";
}

interface Address {
  street: string;
  houseNumber: number | "";
  zip: string;
  city: string;
  telephoneNumber: string;
  email: string;
}

interface CustomerData extends Address {
  firstName: string;
  lastName: string;
  birthDate: Date;
}

interface SupplierData extends Address {
  name: string;
}
